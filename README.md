# ASP.NET Core, Web Api

`Controllers/ValuesController.cs` is where the code is

## Installing & running

1. Clone the repo
2. Run `dotnet restore` command in the project directory to restore/install the dependecies
3. Run `dotnet run` or `dotnet watch run` to get the API up and running. `watch` is the command that will restart the server automatically on file saves.

The web front end code is at https://glitch.com/edit/#!/join/64f46b67-52c8-4378-aac2-dc68abb98f19.

I used VS code instead of visual studio. As I used asp.net core and I like (used to) VS code more.

Besides "testfakturor" that were provided with the test, I used some of my own invoices to test against, which can be found in "uploadedFiles/own", and I checked some invoices online.

## Steps for a good OCR and parsing:

1) image preprocessing. Techniques like -> Stroke Width Transformation, Clean Background Noise, Enhance Contrast, Enhance Resolution, Binarization, Scaling To The Right Size 300 DPI-600 DPI, Detect the Edges of the document and crop the image to only include the invoice, Rotate, deskew if needed.

Probably something with OpenCV / EmguCV for C# for image processing.

I tried a couple of them but they didn't seem to help that much. Probably because the test invoices were in good format already. But they could receive some image processing to improve the results. I'm sure with more time and experimentation devoted to this I would get better results regarding image preprocessing.


2) OCR

Initially I thought that Google Cloud Vision API OCR would be best. While it was good at recognizing words, it sometimes skipped some symbols, critical numbers weren't parsed correctly and the order of the words is sometimes not the right one. Probably with some image preprocessing and experimentation it could give very good results.

C# tesseract didn't have the accuracy needed out of the box.

I went with tesseract js http://tesseract.projectnaptha.com/ as it produced the best results, recognized the bottom of-, and I think the most important portion of the invoice well. Most of the time the bottom part of the invoice includes total sum, ocr and bankgiro. With English as tessdata it recognized the bottom part where tot sum and ocr resides flawlessly. Specified language to the ocr engine also plays a roll. When using English, symbols are better recognized for ex. a "#". In Swedish it is sometimes recognized as ll or something else.

But Swedish did better at the rest of the info.

I could use English to only parse the bottom part with tot sum and ocr for increased accuracy. As english ocr only needs the bottom part, I could crop the image to that part and feed it into the tesseract js ocr for faster processing. Scanning only a certain part of the document to enhance performance and to only parse the relevant info. The image can be cropped to the needed region of the image or it could be done, if possible, by specifying for the ocr engine to only scan that part with coordinates. 

But I managed to get it done in Swedish by not relying on "#" and for this test I thought it would be enough.

Coordinates from already processed ocr text could be used, as described in the test description, to get or validate the data.

Multiple OCR engines can be used to cross check. Some ocr's are better at one thing and other are better for other things. 

Tesseract js can also be used in node js. I could create an OCR web api, which would be more suitable in production when having both mobile and web apps, but for the sake of the test I think that in browser ocr is enough.

Recognizing different layouts by the common symbols, lines etc could be done. For ex./maybe by using EmguCV. Also, the user input could help with that. And training AI to recognize diff. layouts.

3) post-processing of the ocr text. Spell checker/corrector for ex. bing spell check api https://azure.microsoft.com/en-us/services/cognitive-services/spell-check/, 

Or something like https://github.com/LanguageMachines/PICCL

4) Extracting the text by finding keywords, regex and coordinates. In a more advanced version... with the help of ML. The strategy I chose is to do as much as possible without the need of the user input. After processing the user could change the output, mark the non-found info etc. I identified the tot sum, ocr and bg as the most important values.

Besides the fields described in the test description. Additional fields that could be parsed could be "betalningsmottagare" och "betala innan datum" "Oss tillhanda". The name of the company could be parsed through an api by seraching with org. nr. Which might be more accurate. Or used for cross checking of the parsed from ocr name.

detection of false positives by searching for a keyword around the found info for all parsed info as I did with moms, faktura nr. I could also incorporate coordinates. Tests could also be implemented. 

One could also index the text and use some search APIs/engines/libraries to find needed info. For the smart searchable text where for example synonyms or similar words are picked up. Something like -> https://azure.microsoft.com/en-us/blog/how-to-leverage-ocr-to-full-text-search-your-images-within-azure-search/. But then you would also need handle non-searchable text because of non-explicitly written text that for. ex. doesn't have the "name" in-front of the actual names but just has the name itself. Maybe with AI training.

Incorporation of ML that learns from the data and automates the extraction.

Some great resources:
https://blog.altoros.com/optical-character-recognition-using-one-shot-learning-rnn-and-tensorflow.html

https://dzone.com/articles/using-ocr-for-receipt-recognition

https://blogs.dropbox.com/tech/2017/04/creating-a-modern-ocr-pipeline-using-computer-vision-and-deep-learning/

5) let the user select the areas where the not-found by OCR info resides. Also If the layout is different from the majority of invoices or previously unknown for the program layout.

---

The code could be deployed to Azure Functions and used as API over the web.