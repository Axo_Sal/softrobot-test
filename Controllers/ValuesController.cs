﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Drawing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace asp.net_core_web_api.Controllers
{
  [Route("api/[controller]")]
  public class ValuesController : Controller
  {
    public class TotSumAndOCR
    {
      public string TotSum { get; set; }
      public string OCR { get; set; }
    }

    public class RestOfTheInfo
    {
      public string BG { get; set; }
      public string PG { get; set; }
      public string OrgNr { get; set; }
      public string FakturaNr { get; set; }
      public string MomsInPercent { get; set; }
      public string MomsInMoney { get; set; }
      public string Netto { get; set; }
    }

    public class AllInfo : RestOfTheInfo
    {
      public string TotSum { get; set; }
      public string OCR { get; set; }
    }

    // POST api/values
    [HttpPost]
    [Produces("application/json")]
    public IActionResult Post([FromBody]object value)
    {
      var result = JsonConvert.DeserializeObject<dynamic>(value.ToString());

      // if (result["OCRresult"]["eng"] != null)
      // {
      //   string textResult = result.OCRresult.eng;
      // string textResult = result.OCRresult.text;


      // return new OkObjectResult(new TotSumAndOCR { TotSum = totSum, OCR = ocr });
      // return Json(totSum);

      // } // eng end

      // if (result["OCRresult"]["swe"] != null)
      // {
      // Console.WriteLine(result);
      string totSum = "";
      string ocr = "";
      string BG = "";
      string PG = "";
      string OrgNr = "";
      string FakturaNr = "";
      string MomsInPercent = "";
      string MomsInMoney = "";
      string Netto = "";

      // string text = result.OCRresult.swe.text;
      string text = result.OCRresult.text;

      string totSumPattern = @"\d+\s\d{2}\s\d{1}\s\>";

      Match matchedTotSum = Regex.Match(text, totSumPattern);

      if (matchedTotSum.Success)
      {
        string totSumValue = matchedTotSum.Value.Remove(matchedTotSum.Value.Length - 4).Replace(" ", ","); ;

        // Console.WriteLine("Tot Sum " + totSumValue);
        totSum = totSumValue;

        int GetNthIndexWithSearchFromTheEnd(string s, int startFromTheEnd, char character, int n)
        {
          int count = 0;
          for (int i = startFromTheEnd; i >= 0; i--)
          {
            if (s[i] == character)
            {
              count++;
              if (count == n)
              {
                return i;
              }
            }
          }
          return -1;
        }

        int totSumIndex = text.IndexOf(matchedTotSum.Value);

        string ocrPreSubStr = text.Substring(totSumIndex - 50, 50);

        int startFromTheEndIndexOfOCR = ocrPreSubStr.Length - 1;

        int ocrFirstNumIndex = GetNthIndexWithSearchFromTheEnd(ocrPreSubStr, startFromTheEndIndexOfOCR, ' ', 3) + 1;
        int ocrLastNumIndex = GetNthIndexWithSearchFromTheEnd(ocrPreSubStr, startFromTheEndIndexOfOCR, ' ', 2);

        int lengthOfOCRSubStr = ocrLastNumIndex - ocrFirstNumIndex;
        string foundOCR = ocrPreSubStr.Substring(ocrFirstNumIndex, lengthOfOCRSubStr);
        ocr = foundOCR;

        // fails when OCR missidentifies characters as numbers within radius of 50
        // string ocrPattern = @"\s\d+\s";
        // Match matchedOCR = Regex.Match(ocrPreSubStr, ocrPattern);

        // if (matchedOCR.Success)
        // {
        //   string ocrValue = matchedOCR.Value.Replace(" ", "");
        //   // Console.WriteLine("OCR " + ocrValue);
        //   ocr = ocrValue;
        // }

      }
      else
      {
        int attBetalaIndex = text.ToLower().IndexOf("att betala");
        string attBetalaSubStr = text.Substring(attBetalaIndex, 50);
        string attBetalaTotSumPattern = @"\d+\,\d{2}";
        Match matchedAttBetalaTotSum = Regex.Match(attBetalaSubStr, attBetalaTotSumPattern);
        if (matchedAttBetalaTotSum.Success)
        {
          // Console.WriteLine("Att betala " + matchedAttBetalaTotSum.Value);
          totSum = matchedAttBetalaTotSum.Value;
        }

        string ocrPattern = @"\d+";

        int OCRindex = text.IndexOf("OCR");
        if (OCRindex != -1)
        {

          Match matchedOCR = Regex.Match(text.Substring(OCRindex, 50).Replace(" ", ""), ocrPattern);
          if (matchedOCR.Success)
          {
            ocr = matchedOCR.Value;
          }
        }

        int meddelandeIndex = text.ToLower().IndexOf("meddelande");

        if (meddelandeIndex != -1 && OCRindex == -1)
        {
          Match matchedMeddelande = Regex.Match(text.Substring(meddelandeIndex, 50).Replace(" ", ""), ocrPattern);
          if (matchedMeddelande.Success)
          {
            ocr = matchedMeddelande.Value;
          }
        }

      }

      // Org Nr
      string orgNrPattern = @"(?<![0-9.])([0-9]{6}-[0-9]{4})(?![0-9.])";

      Match matchedOrgNr = Regex.Match(text, orgNrPattern);

      if (matchedOrgNr.Success)
      {
        // Console.WriteLine("Org Nr " + matchedOrgNr.Value);
        OrgNr = matchedOrgNr.Value;
      }
      // end of Org Nr

      // PG
      // doesn't work
      // List<string> pgPatterns = new List<string> {
      //   "((?<![0-9.])([0-9]{6}-[0-9]{1})(?![0-9.]))",
      //   "((?<![0-9.])([0-9]{7}-[0-9]{1})(?![0-9.]))",
      //   "((?<![0-9.])(\\d{2}\\s\\d{2}\\s\\d{2}-[0-9]{1})(?![0-9.]))",
      //   "((?<![0-9.])(\\d{3}\\s\\d{2}\\s\\d{2}-[0-9]{1})(?![0-9.]))"
      // };
      // string pgPattern = "@\"" + string.Join("|", pgPatterns.Select(w => w )) + "\"";

      List<string> pgPatterns = new List<string> {
        @"(?<![0-9.])([0-9]{6}-[0-9]{1})(?![0-9.])",
        @"(?<![0-9.])([0-9]{7}-[0-9]{1})(?![0-9.])",
        @"(?<![0-9.])(\d{2}\s\d{2}\s\d{2}-[0-9]{1})(?![0-9.])",
        @"(?<![0-9.])(\d{3}\s\d{2}\s\d{2}-[0-9]{1})(?![0-9.])"
      };

      foreach (string pgPattern in pgPatterns)
      {
        // Console.WriteLine(pgPattern);
        // string pattern = "@\"" + pgPattern + "\"";
        Match matchedPG = Regex.Match(text, pgPattern);

        if (matchedPG.Success)
        {
          // Console.WriteLine("PG " + matchedPG.Value);
          PG = matchedPG.Value;
          break;
        }
      }
      // end of PG

      // MOMS in % and calculate Moms in money and netto from that and total sum previously extracted.
      string momsPattern1 = @"\d{1,2}%";
      string momsPattern2 = @"\d{1,2} %";
      if (foundMoms(momsPattern1) == false)
      {
        foundMoms(momsPattern2);
      }

      bool foundMoms(string momsPattern)
      {
        MatchCollection matchedMoms = Regex.Matches(text, momsPattern);

        foreach (Match m in matchedMoms)
        {
          // Console.WriteLine(m.Value);

          int whereNumInPercentageWasFound = text.IndexOf(m.Value);

          if (whereNumInPercentageWasFound < 50)
          {
            continue;
          }

          // Console.WriteLine("Where " + whereNumInPercentageWasFound);

          int startIndexForMomsSearch = whereNumInPercentageWasFound - 50;

          string momsSubStr = text.Substring(startIndexForMomsSearch, whereNumInPercentageWasFound - startIndexForMomsSearch);

          int isThereMomsWord = momsSubStr.ToLower().IndexOf("moms");

          bool thereIsWordMoms = isThereMomsWord != -1;

          if (thereIsWordMoms)
          {
            // Console.WriteLine("Moms in percent " + m.Value);
            MomsInPercent = m.Value;
            // netto * 1.momsInPercent = totSum
            float momsPercantageIncrease = float.Parse("1." + m.Value.Replace(" ", "").Replace("%", ""));

            // Console.WriteLine("GLOBAL totSum" + totSum); // prints nothing becuase the state between calls is not persistent
            // Console.WriteLine(result.OCRresult.swe.totSum);
            // Console.WriteLine(momsPercantageIncrease);

            // float totSumNum = result.OCRresult.swe.totSum;

            float totSumNum = float.Parse(totSum.Replace(",", "."));
            // Console.WriteLine(totSumNum);
            float netto = totSumNum / momsPercantageIncrease;
            float momsInMoney = totSumNum - netto;

            // Console.WriteLine("NETTO");
            // Console.WriteLine(netto);
            Netto = Convert.ToString(netto);

            // Console.WriteLine("MOMS IN MONEY");
            // Console.WriteLine(momsInMoney);
            MomsInMoney = Convert.ToString(momsInMoney);
            return true;
            // break;
          }
          else
          {
            continue;
          }

        }
        return false;
      }
      // end of MOMS

      // Faktura Nr
      foundFakturaNr("fakturanr");
      foundFakturaNr("fakturanummer");

      void foundFakturaNr(string fakturaNr)
      {
        int fakturaNrIndex = text.ToLower().IndexOf(fakturaNr);

        if (fakturaNrIndex != -1)
        {
          string fakturaNrSubStr = text.Substring(fakturaNrIndex, 70);
          MatchCollection matchedFakturaNr = Regex.Matches(fakturaNrSubStr, @"(?<![0-9.])(\d+)(?![0-9.])");
          foreach (Match m in matchedFakturaNr)
          {
            if (m.Value == ocr)
            {
              FakturaNr = m.Value;
              continue;
            }
            FakturaNr = m.Value;
            break;
          }
        }
      }
      // end of Faktura Nr

      // var words = result.OCRresult.swe.words;

      var words = result.OCRresult.words;

      // going through the grouped words found by ocr because ungrouped, plain text is harder to search because sometimes ocr doesn't produce reliable results when searching in free text. Grouped / boxed results are easier and more relible in the case of BG.

      // BG
      for (int i = words.Count - 1; i >= 0; i--)
      {
        // Console.WriteLine(words[i]);

        string bgPattern = @"(?<![0-9.])([0-9]{3,4}-[0-9]{4})(?![0-9.])";

        string texts = words[i];

        Match matchReg = Regex.Match(texts, bgPattern);

        if (matchReg.Success)
        {
          // Console.WriteLine("BG " + matchReg.Value);
          BG = matchReg.Value;
          break;
        }
      }
      // end of BG

      // return new OkObjectResult(new RestOfTheInfo 
      // {
      //   BG = BG,
      //   PG = PG,
      //   OrgNr = OrgNr,
      //   FakturaNr = FakturaNr,
      //   MomsInPercent = MomsInPercent,
      //   MomsInMoney = MomsInMoney,
      //   Netto = Netto
      // });

      return new OkObjectResult(new AllInfo
      {
        TotSum = totSum,
        OCR = ocr,
        BG = BG,
        PG = PG,
        OrgNr = OrgNr,
        FakturaNr = FakturaNr,
        MomsInPercent = MomsInPercent,
        MomsInMoney = MomsInMoney,
        Netto = Netto
      });

      // } // if swe end

      // return Json("test");
    }

  }
}