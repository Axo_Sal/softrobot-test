// using System;
// using System.IO;
// using System.Web;
// // using System.Web.Http;

// using System.Collections.Generic;
// using System.Net;
// using System.Net.Http;
// using System.Threading.Tasks;
// using Microsoft.AspNetCore.Mvc;

// // using Microsoft.AspNetCore.Http;

// namespace WebApiExample.Controllers
// {
//     public class UploadController : ControllerBase
//     {

//         //public void Post()
//         //{
//         //    Debug.WriteLine(Request.Content);
//         //}

//         public Task<HttpResponseMessage> Post()
//         {


//             List<string> savedFilePath = new List<string>();
//             if (!Request.Content.IsMimeMultipartContent())
//             {
//                 throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
//             }
//             string rootPath = HttpContext.Current.Server.MapPath("~/uploadedFiles");
//             var provider = new MultipartFileStreamProvider(rootPath);
//             string imgPath = "";
//             var task = Request.Content.ReadAsMultipartAsync(provider).
//             ContinueWith<HttpResponseMessage>(t =>
//             {
//                 if (t.IsCanceled || t.IsFaulted)
//                 {
//                     Request.CreateErrorResponse(HttpStatusCode.InternalServerError, t.Exception);
//                 }
//                 foreach (MultipartFileData item in provider.FileData)
//                 {
//                     try
//                     {
//                         string name = item.Headers.ContentDisposition.FileName.Replace("\"", "");
//                         string newFileName = Guid.NewGuid() + Path.GetExtension(name);
//                         imgPath = Path.Combine(rootPath, newFileName);
//                         File.Move(item.LocalFileName, imgPath);

//                         Uri baseuri = new Uri(Request.RequestUri.AbsoluteUri.Replace(Request.RequestUri.PathAndQuery, string.Empty));
//                         string fileRelativePath = "~/uploadedFiles/" + newFileName;
//                         Uri fileFullPath = new Uri(baseuri, VirtualPathUtility.ToAbsolute(fileRelativePath));
//                         savedFilePath.Add(fileFullPath.ToString());
//                     }
//                     catch (Exception ex)
//                     {
//                         string message = ex.Message;
//                     }
//                 }
                
//                 Debug.WriteLine("IMG PATH " + imgPath);

//                 System.Drawing.Image image = System.Drawing.Image.FromFile(@imgPath);

//                 string tiffPath = Path.Combine(rootPath, Guid.NewGuid() + ".tiff");

//                 Debug.WriteLine("TIFF PATH " + tiffPath);

//                 image.Save(@tiffPath, System.Drawing.Imaging.ImageFormat.Tiff);

//                 return Request.CreateResponse(HttpStatusCode.Created, savedFilePath);
//             });
//             return task;
//         }
//     }
// }