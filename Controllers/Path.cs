
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace IHostingEnvironmentNS.Controllers
{
    public class PathController : Controller
    {
        private IHostingEnvironment _env;
        public PathController(IHostingEnvironment env)
        {
            _env = env;
        }
        public string Index(string fileNameAndExt)
        {
            var webRoot = _env.WebRootPath;
            var filePath = System.IO.Path.Combine(webRoot, fileNameAndExt);
            return filePath;
        }
    }
}